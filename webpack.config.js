const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractTextPlugin = new ExtractTextPlugin('style.css');
const webpack = require("webpack");
const path = require("path");

module.exports = {
    entry: "./src/app.tsx",

    output: {
        path: __dirname + "/dist/assets",
        publicPath: '/',
        filename: 'bundle.js'
    },

    devtool: "source-map",

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx", ".json"],
        alias: {
            "react": "React",
            "react-dom": "ReactDOM"
        },
        modules: [
            path.resolve('./'),
            path.resolve('./node_modules'),
        ]
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: ["react-hot-loader/webpack", "babel-loader", "awesome-typescript-loader"],
                exclude: /node_modules/,
            },
            {
                enforce: "pre",
                test: /\.js$/,
                loader: ["source-map-loader"],
                exclude: /node_modules/,
            },

            {
                test: /\.scss$/,
                loader: extractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader?minimize&-autoprefixer!postcss-loader!sass-loader' }),
                // loader: extractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader' }),
            },
            //
            // {
            //     test: /\.css$/,
            //     loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader' }),
            // },
        ]
    },

    plugins: [
        extractTextPlugin,

        // new webpack.optimize.CommonsChunkPlugin({
        //     name: 'vendor',
        //     filename: 'vendor.js',
        //     minChunks(module, count) {
        //         var context = module.context;
        //         return context && context.indexOf('node_modules') >= 0;
        //     },
        // }),

        new webpack.ProvidePlugin({
            "React": "react",
            "ReactDOM": "react-dom",
        }),
    ],



    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
};