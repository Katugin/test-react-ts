'use strict';

const path = require('path');

// Get root directory
const ROOT = path.resolve(__dirname, '..');
const root = (args) => ROOT + '/' + args;


module.exports = {
    root
};
