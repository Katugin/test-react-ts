import * as React from "react";
import * as ReactDOM from "react-dom";

import "./styles/main.scss";
import {Index} from "./components/index";

ReactDOM.render(
    <Index />,
    document.getElementById("main")
);
