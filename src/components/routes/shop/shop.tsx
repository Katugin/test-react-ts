import * as React from "React";
import {RouteComponentProps} from "react-router";

export interface IRouteShop extends RouteComponentProps<any> {

}

export class RouteShop extends React.Component<IRouteShop, undefined> {
    render () {
        return <h1>Shop Route</h1>;
    }
}
