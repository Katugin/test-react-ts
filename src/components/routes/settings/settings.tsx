import * as React from "React";
import {RouteComponentProps} from "react-router";

export interface IRouteSettings extends RouteComponentProps<any> {

}

export class RouteSettings extends React.Component<IRouteSettings, undefined> {
    render () {
        return <h1>Settings route</h1>;
    }
}
