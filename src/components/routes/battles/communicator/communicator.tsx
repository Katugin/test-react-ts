import * as React from "React";
import {NewsData} from "../../../../constants/newsData";


export interface IRouteBattlesCommunicator {

}

export interface IRouteBattlesCommunicatorState {
    newsDate: string;
    newsText: string;
}

export class RouteBattlesCommunicator extends React.Component<IRouteBattlesCommunicator, IRouteBattlesCommunicatorState> {
    constructor (props: any) {
        super(props);

        this.state = {
            newsDate: NewsData.newsData.data[0].date,
            newsText: NewsData.newsData.data[0].text,
        };
    }

    private counter: number = 0;
    private defaultChangeNewsTimer: number = 5000;

    componentDidMount () {
        // Передаю es 6 arrow функцией, чтоб не терять контекст
        var intervalId = setInterval(() => {
            this.timer ()
        }, this.defaultChangeNewsTimer);
    };

    timer () {
        if (this.counter >= NewsData.newsData.data.length)
            this.counter = 0;

        this.setState({
            newsDate: NewsData.newsData.data[this.counter].date,
            newsText: NewsData.newsData.data[this.counter].text
        });

        this.counter += 1;
    };


    render () {
        return <div className="battles__element">
            <div className="battles__header">Comminicator</div>
            <div className="battles__top">
                <div className="ap-button">News</div>
            </div>
            <div className="battles__news">
                <div className="battles__news-title">
                    {this.state.newsDate}
                </div>
                <div className="battles__news-text">
                    {this.state.newsText}
                </div>
            </div>
        </div>;
    }
}
