import * as React from "React";
import {RouteComponentProps} from "react-router";

import {BattlesData, IBattle, IInfoPlayers} from "../../../../constants/battlesData";

export interface IRouteBattlesCurrent {
}

export interface IRouteBattlesCurrentState {
    battlesData: IBattle[];
    sortedData: IRouteBattlesCurrentSorted;
    currentBattle: IBattle;
}

export interface IRouteBattlesCurrentSorted {
    name: boolean,
    info_players: boolean,
    left_time: boolean,
}

export class RouteBattlesCurrent extends React.Component<IRouteBattlesCurrent, IRouteBattlesCurrentState> {
    constructor (props: any) {
        super(props);

        this.state = {
            battlesData: BattlesData.data.data,
            sortedData: this.sortedData,
            currentBattle: BattlesData.data.data[0],
        };
    }

    private sortedData: IRouteBattlesCurrentSorted = {
        name: false,
        info_players: false,
        left_time: false,
    };

    private isFullAll (current: number, max: number): string {
        return current === max
            ? 'count-players__all count-players__all--full'
            : 'count-players__all';
    }

    private isFullRed (current: number, max: number): string {
        return current === max
            ? 'count-players__red count-players__red--full'
            : 'count-players__red';
    }

    private isFullBlue (current: number, max: number): string {
        return current === max
            ? 'count-players__blue count-players__blue--full'
            : 'count-players__blue';
    }

    private isAnyFull (players: IInfoPlayers): string {
        if (players.all !== undefined) {
            return players.all.count_players === players.all.max_players
                ? 'battles__battle battles__battle--full'
                : 'battles__battle';
        } else {
            return players.red.count_players === players.red.max_players
                && players.blue.count_players === players.blue.max_players
                ? 'battles__battle battles__battle--full'
                : 'battles__battle';
        }
    }

    private countPlayers (players: IInfoPlayers) {
        if (players.all !== undefined) {
            return <div className={this.isFullAll(players.all.count_players, players.all.max_players)}>
                {players.all.count_players}
            </div>;
        } else {
            return <div className="battles__battle-count-players battles__battle-count-players--split">
                <div className={this.isFullRed(players.red.count_players, players.red.max_players)}>
                    {players.red.count_players}
                </div>
                <div className={this.isFullBlue(players.blue.count_players, players.blue.max_players)}>
                    {players.blue.count_players}
                </div>
            </div>;
        }
    }

    private selectBattle (id: number): void {
        const currentBattle = this.state.battlesData.filter(x => x.id === id)[0];

        location.search = `battleId=${id}`;

        this.setState({
            currentBattle
        });
    }

    private battlesList (): any {
        const battles: any = [];

        this.state.battlesData.forEach((x) => {
            battles.push(<div className={this.isAnyFull(x.info_players)} key={x.id} onClick={this.selectBattle.bind(this, x.id)}>
                <div className="battles__battle-name">{x.name}</div>
                <div className="battles__battle-count-players">
                    {this.countPlayers(x.info_players)}
                </div>
                <div className="battles__battle-time">{x.left_time}</div>
            </div>);
        });

        return battles;
    }

    private sort(type: string): void {
        const data = this.state.battlesData;
        const isSorted = this.sortedData.name;
        let direction = isSorted ? 1 : -1;

        if (type === 'name') {
            data.sort((a: IBattle, b: IBattle) => {
                let direction = isSorted ? 1 : -1;

                if (a[type] === b[type]) { return 0; }

                return a[type] > b[type] ? direction : direction * -1;
            });

            this.sortedData.name = !isSorted;
        }

        if (type === 'left_time') {
            data.sort((a: IBattle, b: IBattle) => {
                let direction = isSorted ? 1 : -1;

                if (a[type] === b[type]) { return 0; }

                return a[type] > b[type] ? direction : direction * -1;
            });

            this.sortedData.name = !isSorted;
        }

        if (type === 'info_players') {
            data.sort((a: IBattle, b: IBattle) => {
                let direction = isSorted ? 1 : -1;

                if (a[type].all !== undefined && b[type].all !== undefined) {
                    if (a[type].all.count_players === b[type].all.count_players) { return 0; }

                    return a[type].all.count_players > b[type].all.count_players ? direction : direction * -1;
                } else if (a[type].blue !== undefined && b[type].blue !== undefined) {
                    if (a[type].blue.count_players === b[type].blue.count_players) { return 0; }

                    return a[type].blue.count_players > b[type].blue.count_players ? direction : direction * -1;
                } else if (a[type].red !== undefined && b[type].red !== undefined) {
                    if (a[type].red.count_players === b[type].red.count_players) { return 0; }

                    return a[type].red.count_players > b[type].red.count_players ? direction : direction * -1;
                }
            });

            this.sortedData.name = !isSorted;
        }

        this.setState({
            battlesData: data,
            sortedData: this.state.sortedData,
        })
    }

    render () {
        return <div className="battles__element">
            <div className="battles__header">Current Battles</div>
            <div className="battles__top battles__top--center">
                <div className="ap-button ap-button--battle">Battle!</div>
            </div>
            <div className="battles__container">
                {this.battlesList()}
            </div>
            <div className="battles__bottom">
                <div className="battles__bottom-element" onClick={this.sort.bind(this, 'name')}>Sort name</div>
                <div className="battles__bottom-element" onClick={this.sort.bind(this, 'info_players')}>Sort ready</div>
                <div className="battles__bottom-element" onClick={this.sort.bind(this, 'left_time')}>Sort time</div>
            </div>
        </div>;
    }
}
