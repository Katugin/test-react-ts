import * as React from "React";
import {RouteComponentProps} from "react-router";
import {BattlesData, IBattle, IInfoPlayers} from "../../../../constants/battlesData";

export interface IRouteBattlesInfo {
    location: any;
}

export interface IRouteBattlesInfoState {
}

export class RouteBattlesInfo extends React.Component<any, IRouteBattlesInfoState> {
    constructor (props: any) {
        super(props);
    }

    private getBattleInfoById (): IBattle{
        const search: string = location.search;
        if (search !== '' && search.indexOf('battleId') > -1) {
            return BattlesData.data.data[parseInt(search.split('=')[1]) - 1];
        } else {
            return BattlesData.data.data[0];
        }
    }

    private getRedPlayers (players: IInfoPlayers): any {
        const list: any = [];

        players.red.players.forEach((x) => {
            list.push(<div className="player-info" key={x.name}>
                <div className="player-info-name">{x.name}</div>
                <div className="player-info-kills">{x.kills}</div>
            </div>)
        });

        let notFullPlayer = players.red.max_players - list.length;
        for (notFullPlayer; notFullPlayer > 0; notFullPlayer -= 1) {
            list.push(<div className="player-info" key={notFullPlayer}>
                <div className="player-info-name player-info-name--none">none</div>
            </div>)
        }

        return list;
    }

    private getBluePlayers (players: IInfoPlayers): any {
        const list: any = [];

        players.blue.players.forEach((x) => {
            list.push(<div className="player-info" key={x.name}>
                <div className="player-info-name">{x.name}</div>
                <div className="player-info-kills">{x.kills}</div>
            </div>)
        });

        let notFullPlayer = players.blue.max_players - list.length;
        for (notFullPlayer; notFullPlayer > 0; notFullPlayer -= 1) {
            list.push(<div className="player-info" key={notFullPlayer}>
                <div className="player-info-name player-info-name--none">none</div>
            </div>)
        }

        return list;
    }

    private getPlayers (players: IInfoPlayers): any {
        if (players.all !== undefined) {
            const list: any = [];

            players.all.players.forEach((x) => {
                list.push(<div className="player-info" key={x.name}>
                    <div className="player-info-name">{x.name}</div>
                    <div className="player-info-kills">{x.kills}</div>
                </div>)
            });

            let notFullPlayer = players.all.max_players - list.length;
            for (notFullPlayer; notFullPlayer > 0; notFullPlayer -= 1) {
                list.push(<div className="player-info" key={notFullPlayer}>
                    <div className="player-info-name player-info-name--none">none</div>
                </div>)
            }

            return list;
        } else {
            return <div className="player-info player-info--two">
                <div className="player-info--red">{this.getRedPlayers(this.getBattleInfoById().info_players)}</div>
                <div className="player-info--blue">{this.getBluePlayers(this.getBattleInfoById().info_players)}</div>
            </div>
        }
    }

    render () {
        return <div className="battles__element">
            <div className="battles__header">Battle Info</div>
            <div className="battles__info">
                <div className="battles__info-link">{this.getBattleInfoById().link}{this.getBattleInfoById().id}</div>
                <div className="ap-button ap-button--gray ap-button--info">copy</div>
            </div>
            <div className="battles__battle-info">
                {this.getPlayers(this.getBattleInfoById().info_players)}
            </div>
        </div>;
    }
}
