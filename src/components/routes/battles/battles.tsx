import * as React from "React";
import {RouteComponentProps} from "react-router";
import {RouteBattlesCommunicator} from "./communicator/communicator";
import {RouteBattlesCurrent} from "./currentBattles/current";
import {RouteBattlesInfo} from "./info/info";

export interface IRouteBattles extends RouteComponentProps<any> {

}

export class RouteBattles extends React.Component<IRouteBattles, undefined> {
    render () {
        return <div className="routes">
            <RouteBattlesCommunicator></RouteBattlesCommunicator>
            <RouteBattlesCurrent></RouteBattlesCurrent>
            <RouteBattlesInfo></RouteBattlesInfo>
        </div>;
    }
}
