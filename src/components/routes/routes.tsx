import * as React from "react";
import {
    BrowserRouter as Router,
    Route,
    Link
} from "react-router-dom";

import {RoutesConstant} from "../../constants/routes";
import {RouteBattles} from "./battles/battles";
import {RouteShop} from "./shop/shop";
import {RouteGarage} from "./garage/garage";
import {RouteSettings} from "./settings/settings";

export interface IRoutes {

}

export class Routes extends React.Component<IRoutes, undefined> {
    render () {
        return <div>
            <Route exact path={RoutesConstant.battles} component={RouteBattles}/>
            <Route path={RoutesConstant.shop} component={RouteShop}/>
            <Route path={RoutesConstant.garage} component={RouteGarage}/>
            <Route path={RoutesConstant.settings} component={RouteSettings}/>
        </div>;
    }
}