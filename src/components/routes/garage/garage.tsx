import * as React from "React";
import {RouteComponentProps} from "react-router";

export interface IRouteGarage extends RouteComponentProps<any> {

}

export class RouteGarage extends React.Component<IRouteGarage, undefined> {
    render () {
        return <h1>Garage route</h1>;
    }
}
