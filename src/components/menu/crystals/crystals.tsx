import * as React from "React";

export interface IMenuCrystals {
    countCry: number;
}

export class MenuCrystals extends React.Component<IMenuCrystals, undefined> {
    render () {
        return <div className="menu__crystal">Cry {this.props.countCry}</div>;
    }
}
