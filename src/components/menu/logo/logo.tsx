import * as React from "React";

export interface IMenuLogo {

}

export class MenuLogo extends React.Component<IMenuLogo, undefined> {
    render () {
        return <img src="./img/logo.png" className="menu__logo"/>;
    }
}
