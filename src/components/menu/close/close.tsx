import * as React from "React";

export interface IMenuClose {

}

export class MenuClose extends React.Component<IMenuClose, undefined> {
    render () {
        return <div className="menu__close">X</div>;
    }
}
