import * as React from "React";

export interface IMenuProgressBar {
    progress: number;
}

export class MenuProgressBar extends React.Component<IMenuProgressBar, undefined> {
    render () {
        return <div className="menu__progress-bar" data-percent={this.props.progress}>Progress: {this.props.progress}</div>;
    }
}
