import * as React from "React";
import {ILangList} from "../../../interfaces/common/ILangList";

export interface IMenuLangSwitch {
    langList: ILangList;
}

export class MenuLangSwitch extends React.Component<IMenuLangSwitch, undefined> {
    render () {
        return <div className="menu__lang">{this.props.langList.ru}</div>;
    }
}
