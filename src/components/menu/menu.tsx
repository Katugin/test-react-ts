import * as React from "react";
import {
    BrowserRouter as Router,
    Link
} from "react-router-dom";
import {MenuLangSwitch} from "./langSwitch/langSwitch";
import {Constant} from "../../constants/constant";
import {MenuProgressBar} from "./progressBar/progressBar";
import {MenuLogo} from "./logo/logo";
import {MenuShop} from "./shop/shop";
import {MenuCrystals} from "./crystals/crystals";
import {MenuBattles} from "./battles/battles";
import {MenuGarage} from "./garage/garage";
import {MenuClose} from "./close/close";
import {MenuSettings} from "./settings/settings";
import {RoutesConstant} from "../../constants/routes";

export interface IMenu {

}

export class Menu extends React.Component<IMenu, undefined> {
    render () {
        return <div className="menu">
            <div className="menu__element">
                <Link to={RoutesConstant.battles}>
                    <MenuLogo></MenuLogo>
                </Link>
            </div>
            <div className="menu__element menu__element--progress">
                <MenuProgressBar progress={20}></MenuProgressBar>
            </div>
            <div className="menu__element">
                <Link to={RoutesConstant.shop}>
                    <MenuCrystals countCry={600}></MenuCrystals>
                </Link>
            </div>
            <div className="menu__element">
                <Link to={RoutesConstant.shop}>
                    <MenuShop></MenuShop>
                </Link>
            </div>
            <div className="menu__element">
                <Link to={RoutesConstant.battles}>
                    <MenuBattles></MenuBattles>
                </Link>
            </div>
            <div className="menu__element">
                <Link to={RoutesConstant.garage}>
                    <MenuGarage></MenuGarage>
                </Link>
            </div>
            <div className="menu__element">
                <MenuLangSwitch langList={Constant.langList}></MenuLangSwitch>
            </div>
            <div className="menu__element">
                <Link to={RoutesConstant.settings}>
                    <MenuSettings></MenuSettings>
                </Link>
            </div>
            <div className="menu__element">
                <MenuClose></MenuClose>
            </div>
        </div>;
    }
}
