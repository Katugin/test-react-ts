import * as React from "React";

export interface IMenuGarage {

}

export class MenuGarage extends React.Component<IMenuGarage, undefined> {
    render () {
        return <div className="ap-button ap-button--menu ap-button--gray">Garage</div>;
    }
}
