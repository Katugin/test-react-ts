import * as React from "react";
import {
    BrowserRouter as Router,
} from "react-router-dom";
import {Menu} from "./menu/menu";
import {Routes} from "./routes/routes";

export interface IIndex {

}

export class Index extends React.Component<IIndex, undefined> {
    render () {
        return <Router>
            <switch>
                <Menu></Menu>
                <Routes></Routes>
            </switch>
        </Router>;
    }
}
