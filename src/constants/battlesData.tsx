export interface IPlayer {
    name: string;
    kills: number;
}

export interface ICountPlayers {
    count_players: number;
    max_players: number;
    players: IPlayer[];
}

export interface IInfoPlayers {
    blue?: ICountPlayers;
    red?: ICountPlayers;
    all?: ICountPlayers
}

export interface IBattle {
    id: number;
    name: string;
    info_players: IInfoPlayers;
    left_time: string;
    link: string;
}

export interface IBattlesData {
    data: IBattle[];
}

export class BattlesData {
    static data: IBattlesData = {
        data: [
            {
                id: 1,
                name: 'A TDM',
                info_players: {
                    blue: {
                        count_players: 3,
                        max_players: 5,
                        players: [
                            {
                                name: 'test guy 1',
                                kills: 3,
                            },
                            {
                                name: 'mega Guy',
                                kills: 2,
                            },
                            {
                                name: 'True Guy',
                                kills: 0,
                            }
                        ]
                    },
                    red: {
                        count_players: 4,
                        max_players: 5,
                        players: [
                            {
                                name: 'Rex',
                                kills: 3,
                            },
                            {
                                name: 'Guli',
                                kills: 1,
                            },
                            {
                                name: 'Mordor',
                                kills: 0,
                            },
                            {
                                name: 'NagibatorXXX',
                                kills: 0,
                            }
                        ]
                    }
                },
                left_time: '15:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 2,
                name: 'B TDM',
                info_players: {
                    blue: {
                        count_players: 5,
                        max_players: 5,
                        players: [
                            {
                                name: 'Vaste',
                                kills: 3,
                            },
                            {
                                name: 'Keke',
                                kills: 3,
                            },
                            {
                                name: 'Sima',
                                kills: 0,
                            },
                            {
                                name: 'Dikiy',
                                kills: 3,
                            },
                            {
                                name: 'Moped',
                                kills: 0,
                            },
                        ]
                    },
                    red: {
                        count_players: 5,
                        max_players: 5,
                        players: [
                            {
                                name: 'Swet',
                                kills: 11,
                            },
                            {
                                name: 'Wel',
                                kills: 0,
                            },
                            {
                                name: 'TheRak',
                                kills: 0,
                            },
                            {
                                name: 'Paris',
                                kills: 12,
                            },
                            {
                                name: 'Irland',
                                kills: 0,
                            },
                        ]
                    }
                },
                left_time: '10:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 3,
                name: 'C TDM',
                info_players: {
                    blue: {
                        count_players: 5,
                        max_players: 5,
                        players: [
                            {
                                name: 'Guru',
                                kills: 3,
                            },
                            {
                                name: 'GameManiak',
                                kills: 0,
                            },
                            {
                                name: 'Someone',
                                kills: 2,
                            },
                            {
                                name: 'Flash',
                                kills: 0,
                            },
                            {
                                name: 'Gorrila',
                                kills: 5,
                            },
                        ]
                    },
                    red: {
                        count_players: 5,
                        max_players: 5,
                        players: [
                            {
                                name: 'Barry',
                                kills: 1,
                            },
                            {
                                name: 'Wally',
                                kills: 0,
                            },
                            {
                                name: 'Iris',
                                kills: 8,
                            },
                            {
                                name: 'John',
                                kills: 0,
                            },
                            {
                                name: 'Harryson',
                                kills: 9,
                            },
                        ]
                    }
                },
                left_time: '10:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 4,
                name: 'Y TDM',
                info_players: {
                    blue: {
                        count_players: 5,
                        max_players: 5,
                        players: [
                            {
                                name: 'Zolomon',
                                kills: 3,
                            },
                            {
                                name: 'EvilWest',
                                kills: 0,
                            },
                            {
                                name: 'Hodor',
                                kills: 6,
                            },
                            {
                                name: 'Deathstroke',
                                kills: 0,
                            },
                            {
                                name: 'Wolverine',
                                kills: 1,
                            },
                        ]
                    },
                    red: {
                        count_players: 5,
                        max_players: 5,
                        players: [
                            {
                                name: 'Octopus',
                                kills: 0,
                            },
                            {
                                name: 'Glup',
                                kills: 4,
                            },
                            {
                                name: 'NoizeMC',
                                kills: 0,
                            },
                            {
                                name: 'Atilla',
                                kills: 0,
                            },
                            {
                                name: 'Rome',
                                kills: 16,
                            },
                        ]
                    }
                },
                left_time: '10:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 5,
                name: 'Scandal TDM',
                info_players: {
                    blue: {
                        count_players: 5,
                        max_players: 10,
                        players: [
                            {
                                name: 'yarn',
                                kills: 3,
                            },
                            {
                                name: 'gulp',
                                kills: 0,
                            },
                            {
                                name: 'jasmime',
                                kills: 55,
                            },
                            {
                                name: 'ecma6',
                                kills: 0,
                            },
                            {
                                name: 'selenium',
                                kills: 4,
                            },
                        ]
                    },
                    red: {
                        count_players: 5,
                        max_players: 10,
                        players: [
                            {
                                name: 'Super',
                                kills: 0,
                            },
                            {
                                name: 'WonderWoman',
                                kills: 54,
                            },
                            {
                                name: 'Cheater',
                                kills: 99,
                            },
                            {
                                name: 'Pobeditel',
                                kills: 0,
                            },
                            {
                                name: 'TruePlayer',
                                kills: 4,
                            },
                        ]
                    }
                },
                left_time: '15:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 6,
                name: 'X CP',
                info_players: {
                    blue: {
                        count_players: 1,
                        max_players: 5,
                        players: [
                            {
                                name: 'NotGay',
                                kills: 3,
                            },
                        ]
                    },
                    red: {
                        count_players: 1,
                        max_players: 5,
                        players: [
                            {
                                name: 'NotAAMan',
                                kills: 0,
                            },
                        ]
                    }
                },
                left_time: '05:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 7,
                name: 'Parkour TDM',
                info_players: {
                    blue: {
                        count_players: 5,
                        max_players: 7,
                        players: [
                            {
                                name: 'Sven',
                                kills: 3,
                            },
                            {
                                name: 'Axe',
                                kills: 0,
                            },
                            {
                                name: 'Rikimaru',
                                kills: 0,
                            },
                            {
                                name: 'Alchemist',
                                kills: 44,
                            },
                            {
                                name: 'Mirana',
                                kills: 0,
                            },
                        ]
                    },
                    red: {
                        count_players: 5,
                        max_players: 7,
                        players: [
                            {
                                name: 'Abbadon',
                                kills: 3,
                            },
                            {
                                name: 'Rubick',
                                kills: 0,
                            },
                            {
                                name: 'Phoenix',
                                kills: 5,
                            },
                            {
                                name: 'Dazzle',
                                kills: 0,
                            },
                            {
                                name: 'LegionCommander',
                                kills: 3,
                            },
                        ]
                    }
                },
                left_time: '10:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 8,
                name: 'Some TDM',
                info_players: {
                    blue: {
                        count_players: 2,
                        max_players: 9,
                        players: [
                            {
                                name: 'Lirik',
                                kills: 3,
                            },
                            {
                                name: 'Twarina',
                                kills: 0,
                            },
                        ]
                    },
                    red: {
                        count_players: 3,
                        max_players: 9,
                        players: [
                            {
                                name: 'WeLoveGames',
                                kills: 0,
                            },
                            {
                                name: 'ILame',
                                kills: 99,
                            },
                            {
                                name: 'Murazor',
                                kills: 0,
                            },
                        ]
                    }
                },
                left_time: '15:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 9,
                name: 'Cross TDM',
                info_players: {
                    blue: {
                        count_players: 7,
                        max_players: 7,
                        players: [

                            {
                                name: 'native',
                                kills: 2,
                            },
                            {
                                name: 'Seven',
                                kills: 7,
                            },
                            {
                                name: 'Hause',
                                kills: 0,
                            },
                            {
                                name: 'Gray',
                                kills: 1,
                            },
                            {
                                name: 'End',
                                kills: 0,
                            },
                            {
                                name: 'Dorian',
                                kills: 1,
                            },
                            {
                                name: 'Malphurion',
                                kills: 0,
                            },
                        ]
                    },
                    red: {
                        count_players: 7,
                        max_players: 7,
                        players: [
                            {
                                name: 'Simba',
                                kills: 3,
                            },
                            {
                                name: 'Avgust',
                                kills: 0,
                            },
                            {
                                name: 'Ironman',
                                kills: 4,
                            },
                            {
                                name: 'German',
                                kills: 0,
                            },
                            {
                                name: 'Alberty',
                                kills: 0,
                            },
                            {
                                name: 'Niko',
                                kills: 0,
                            },
                            {
                                name: 'August',
                                kills: 0,
                            },
                        ]
                    }
                },
                left_time: '10:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 10,
                name: 'Some TDM',
                info_players: {
                    blue: {
                        count_players: 3,
                        max_players: 5,
                        players: [
                            {
                                name: 'Nami',
                                kills: 3,
                            },
                            {
                                name: 'Lucy',
                                kills: 0,
                            },
                            {
                                name: 'Zoro',
                                kills: 0,
                            },
                        ]
                    },
                    red: {
                        count_players: 3,
                        max_players: 5,
                        players: [
                            {
                                name: 'Illidan',
                                kills: 0,
                            },
                            {
                                name: 'Gazlow',
                                kills: 3,
                            },
                            {
                                name: 'SocialProtect',
                                kills: 0,
                            },
                        ]
                    }
                },
                left_time: '10:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 11,
                name: 'Cool DM',
                info_players: {
                    all: {
                        count_players: 5,
                        max_players: 10,
                        players: [
                            {
                                name: 'Nesquick',
                                kills: 3,
                            },
                            {
                                name: 'Azmodan',
                                kills: 0,
                            },
                            {
                                name: 'Probius',
                                kills: 0,
                            },
                            {
                                name: 'Valla',
                                kills: 0,
                            },
                            {
                                name: 'Valira',
                                kills: 0,
                            },
                        ]
                    }
                },
                left_time: '10:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 12,
                name: 'Super Cool DM',
                info_players: {
                    all: {
                        count_players: 10,
                        max_players: 10,
                        players: [
                            {
                                name: 'Russian Guy',
                                kills: 3,
                            },
                            {
                                name: 'English Guy',
                                kills: 0,
                            },
                            {
                                name: 'Jew Guy',
                                kills: 0,
                            },
                            {
                                name: 'Biritsh Guy',
                                kills: 0,
                            },
                            {
                                name: 'Perm Guy',
                                kills: 0,
                            },
                            {
                                name: 'Russian Guy',
                                kills: 3,
                            },
                            {
                                name: 'English Guy',
                                kills: 0,
                            },
                            {
                                name: 'Jew Guy',
                                kills: 0,
                            },
                            {
                                name: 'Biritsh Guy',
                                kills: 0,
                            },
                            {
                                name: 'Perm Guy',
                                kills: 0,
                            },
                        ]
                    }
                },
                left_time: '10:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 13,
                name: '13 TDM',
                info_players: {
                    blue: {
                        count_players: 7,
                        max_players: 10,
                        players: [
                            {
                                name: 'Russian Guy',
                                kills: 3,
                            },
                            {
                                name: 'English Guy',
                                kills: 0,
                            },
                            {
                                name: 'Jew Guy',
                                kills: 0,
                            },
                            {
                                name: 'Biritsh Guy',
                                kills: 0,
                            },
                            {
                                name: 'Perm Guy',
                                kills: 0,
                            },
                        ]
                    },
                    red: {
                        count_players: 10,
                        max_players: 10,
                        players: [
                            {
                                name: 'Oleg',
                                kills: 0,
                            },
                            {
                                name: 'Ivan',
                                kills: 0,
                            },
                            {
                                name: 'Marat',
                                kills: 0,
                            },
                            {
                                name: 'Ignat',
                                kills: 0,
                            },
                            {
                                name: 'Sergey',
                                kills: 0,
                            },
                        ]
                    }
                },
                left_time: '05:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
            {
                id: 14,
                name: 'TDM',
                info_players: {
                    blue: {
                        count_players: 15,
                        max_players: 15,
                        players: [
                            {
                                name: 'Russian Guy',
                                kills: 3,
                            },
                            {
                                name: 'English Guy',
                                kills: 0,
                            },
                            {
                                name: 'Jew Guy',
                                kills: 0,
                            },
                            {
                                name: 'Biritsh Guy',
                                kills: 0,
                            },
                            {
                                name: 'Perm Guy',
                                kills: 0,
                            },
                        ]
                    },
                    red: {
                        count_players: 13,
                        max_players: 15,
                        players: [
                            {
                                name: 'Oleg',
                                kills: 0,
                            },
                            {
                                name: 'Ivan',
                                kills: 0,
                            },
                            {
                                name: 'Marat',
                                kills: 0,
                            },
                            {
                                name: 'Ignat',
                                kills: 0,
                            },
                            {
                                name: 'Sergey',
                                kills: 0,
                            },
                        ]
                    }
                },
                left_time: '10:00',
                link: 'http://tankionline.com/ru/category/tankinews/',
            },
        ]
    };
}