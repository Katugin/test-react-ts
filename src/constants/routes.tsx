export class RoutesConstant {
    static battles: string = '/battles';
    static shop: string = '/shop';
    static garage: string = '/garage';
    static settings: string = '/settings';
}