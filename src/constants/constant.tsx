import {ILangList} from "../interfaces/common/ILangList";
export class Constant {
    static langList: ILangList = {
        ru: 'RU',
        en: 'EN',
    };
}